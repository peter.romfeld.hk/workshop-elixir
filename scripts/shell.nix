with import <nixpkgs> {};

let
  nixpkgs = import (builtins.fetchTarball {
    name = "nixpkgs-unstable-2019-03-27";
    url = "https://github.com/nixos/nixpkgs/archive/0a88820fc950a1415ae8b3afbf9d30c05d1160e0.tar.gz";
    sha256 = "0xmainjkzdbc4n0dgysg4rnwm1nxpbprz46zwap305cqq1n3s7f5";
  }) {};

  my_erlang = nixpkgs.erlangR21.override {
    # HiPE has bad performance for IO, only useful for math
    enableHipe = false;
    # needed for observer
    wxSupport = true;
  };
  my_elixir = (nixpkgs.beam.packagesWith my_erlang).elixir_1_8;

in with nixpkgs; mkShell {
  buildInputs = [
    my_erlang
    my_elixir
    nodejs-10_x

    postgresql_11
    overmind

  ] ++ stdenv.lib.optionals stdenv.isLinux [
    inotify-tools
  ];
}
