# Elixir Workshop

How to make a simple webapp using elixir and phoenixframework

## WebApp

```shell
#!/usr/bin/env fish

# Start a new project (first Y second n)
nix-shell \
  -I nixpkgs=https://github.com/nixos/nixpkgs/archive/0a88820fc950a1415ae8b3afbf9d30c05d1160e0.tar.gz \
  -p elixir_1_8 beamPackages.hex \
  --run 'mix local.hex --force; mix local.rebar --force; mix archive.install hex phx_new; mix phx.new webapp'

cd webapp
for file in (ls -A ../scripts/)
  ln -sf ../scripts/$file $file
end
nix-shell --run fish
mix deps.get
cd assets; npm install; node node_modules/webpack/bin/webpack.js --mode development; cd ..
overmind s -l pg

# PSQL (once)
createuser -h (pwd)/.pgsql_data webapp
createdb -h (pwd)/.pgsql_data -O webapp webapp

sed -i 's/username: ".*"/username: "webapp"/g' config/dev.exs
sed -i '/password: "/d' config/dev.exs
sed -i 's/database: ".*"/database: "webapp"/g' config/dev.exs

iex -S mix phx.server

# after initial setup you can run (to run new db migrations just restart overmind)
overmind s -l pg,app
```

## TDD

* add `{:mix_test_watch, "~> 0.9", only: :dev, runtime: false}` to `deps` in `mix.exs`
* install new dependency with `mix deps.get`
* remove `"ecto.create --quiet",` from `test` `alias`
* add another `alias` in `mix.exs`: `"test.watch": ["ecto.migrate", "test.watch"]`
* add test db:

```shell
# cwd = webapp
# create test_db
overmind s -l pg
createdb -h (pwd)/.pgsql_data -O webapp webapp_test
sed -i 's/username: ".*"/username: "webapp"/g' config/test.exs
sed -i '/password: "/d' config/test.exs
sed -i 's/database: ".*"/database: "webapp_test"/g' config/test.exs
```

* run test watcher with `mix test.watch` or just have `overmind s -l pg,test`
* [coverage](https://github.com/parroty/excoveralls)

## [Property based testing](https://github.com/whatyouhide/stream_data)

## [debugging](https://elixir-lang.org/getting-started/debugging.html)

## [profiling](https://github.com/parroty/exprof)

## WIP

```shell
# create a new schema
## check command options
mix help
mix help phx.gen.json

## scaffold controller/view/schema/migration/tests
mix phx.gen.json Accounts User users email:string

tee priv/repo/seeds.exs >/dev/null <<EOF
defmodule Webapp.DatabaseSeeder do
  alias Webapp.Repo
  alias Webapp.Link

  @titles_list ["Erlang", "Elixir", "Phoenix Framework"] # list of titles
  @urls_list ["http://www.erlang.org", "http://www.elixir-lang.org", "http://www.phoenixframework.org"] # list of urls

  def insert_link do
    Repo.insert! %Link{
      title: (@titles_list |> Enum.random()),
      url: (@urls_list |> Enum.random())
    }
  end

  def clear do
    Repo.delete_all()
  end
end

(1..100) |> Enum.each(fn _ -> Webapp.DatabaseSeeder.insert_link() end)
EOF
```

## Exercises

* [Chat App](https://github.com/dwyl/phoenix-chat-example)
* [LiveView](https://github.com/phoenixframework/phoenix_live_view)

## See also

* https://elixir-lang.org/
* https://phoenixframework.org/
